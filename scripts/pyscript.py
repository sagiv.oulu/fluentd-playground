import sys

input_data_path = sys.argv[-1]

with open(input_data_path, 'r') as input_data:
    lines = input_data.readlines()
    with open('/out/out.txt', 'a') as output:
        for line in lines:
            output.write(f'{str(sys.argv)} -> {line}\n')